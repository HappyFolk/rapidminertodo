DROP ALL OBJECTS;

CREATE SCHEMA RAPIDMINER;
CREATE TABLE RAPIDMINER.TODO
(
    TODO_ID          integer auto_increment,
    SUBJECT          varchar(255) NOT NULL,
    DESCRIPTION      clob         NOT NULL,
    CREATED_BY       varchar(255) NOT NULL,
    CREATED_FOR      varchar(255),
    CREATE_TIMESTAMP timestamp default CURRENT_TIMESTAMP
);