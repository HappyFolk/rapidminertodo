console.log("Hello");

let todoArray = [];
let todoLoaded = 0;
let bottomReached = false;


//TIME out to reduce the amount of REST Calls
window.onscroll = async function (ev) {
    await new Promise(resolve => setTimeout(resolve, 100));
    if (!bottomReached && (window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        bottomReached = true;
        await loadNextTodos();
        console.log("Loaded ", todoLoaded, " todos total");
        bottomReached = false;
    }
};

function loadNextTodos() {

    httpService.nextTodos(todoArray[todoLoaded - 1].todoId).then((result => {
        let values = result.nextTodos;
        let table = document.getElementById("todoTable");

        todoArray = todoArray.concat(values);
        todoLoaded += values.length;

        values.forEach(todo => {
            addTodoRow(todo, table);
        });


        let removed = result.removedIds;
        for (let i = 0; i < todoArray.length; i++) {
            if (removed.includes(todoArray[i].todoId)) {
                console.log("Removing id", todoArray[i].todoId, " from index: ", i);
                todoLoaded--;
                table.deleteRow(i + 1); //header
            }
        }

        todoArray = todoArray.filter(t => !removed.includes(t.todoId));

    }));
    return new Promise(resolve => setTimeout(resolve, 100));
}


function createTodo() {

    let createTodoRequest = {

        subject: document.getElementById('subjectField').value,
        description: document.getElementById('descriptionField').value,
        createdBy: document.getElementById('createdByField').value,
        createdFor: document.getElementById('createdForField').value


    };


    httpService.createTodo(createTodoRequest)
        .then(responnse => {
            console.log(responnse);
            showModal("Created todo", responnse.message);
        })
        .catch(err => {
            console.log(err);
            showModal("Failed to create todo", err.message);
        });

}

function deleteTodo() {
    httpService.deleteTodo(document.getElementById('removeTodoField').value,)
        .then(responnse => {
            console.log(responnse);
            showModal("Removed todo", responnse.message);
        })
        .catch(err => {
            console.log(err);
            showModal("Failed to remove todo", err.message);
        });
}

httpService.initTodos().then((values => {
    let table = document.getElementById("todoTable");

    todoArray = todoArray.concat(values);
    todoLoaded += values.length;

    values.forEach(todo => {
        addTodoRow(todo, table);
    });


}));

function addTodoRow(todo, table) {
    let tableSize = table.rows.length;
    let row = table.insertRow(tableSize);
    let idCell = row.insertCell(0);

    idCell.innerHTML = todo.todoId;
    let subCell = row.insertCell(1);
    subCell.innerHTML = todo.subject;
    let desCell = row.insertCell(2);
    desCell.innerHTML = todo.description;
    let byCell = row.insertCell(3);
    byCell.innerHTML = todo.createdBy;
    let forCell = row.insertCell(4);
    forCell.innerHTML = todo.createdFor;
    let timeCell = row.insertCell(5);
    timeCell.innerHTML = todo.createTime;
}

function showModal(title, text) {
    let myModal = document.getElementById('modalTemplate');

    let myModalInstance = new Modal(myModal,
        {
            content: ' <div class="modal-content">' +
                '            <div class="modal-header">' +
                '                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span' +
                '                        aria-hidden="true">×</span></button>' +
                '                <h5 class="modal-title" id="myModalLabel">' + title + '</h5>' +
                '            </div>' +
                '            <div class="modal-body">' +
                '                ' + text +
                '            </div>' +
                '            <div class="modal-footer">' +
                '                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>' +
                '            </div>' +
                '        </div>',
            backdrop: 'static', // we don't want to dismiss Modal when Modal or backdrop is the click event target
            keyboard: false // we don't want to dismiss Modal on pressing Esc key
        });

    myModalInstance.show();
}
