const httpService = {
    initTodos: async () => {
        console.log("Init todos...");
        const response = await fetch('/todo-endpoint/init-todos', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        let json = await response.json();
        console.log("Received: ", json);
        return json;
    },

    createTodo: async (todo) => {
        console.log("Create todo: ", todo);


        const response = await fetch('/todo-endpoint/create-todo', {
            method: 'POST',
            body: JSON.stringify(todo), // for post
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
        let json = await response.json();
        console.log("Received: ", json);
        if (response.status === 200) {
            return json;
        } else {
            throw json;
        }


    },

    deleteTodo: async (id) => {
        console.log("Delete todo: ", id);
        const response = await fetch('/todo-endpoint/delete-todo?id=' + id, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        let json = await response.json();
        console.log("Received: ", json);
        return json;
    },

    nextTodos: async (lastId) => {
        console.log("Next todos from lastId: ", lastId);
        const response = await fetch('/todo-endpoint/next-todos?lastId=' + lastId, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        let json = await response.json();
        console.log("Received next todos: ", json);
        return json;
    }

};