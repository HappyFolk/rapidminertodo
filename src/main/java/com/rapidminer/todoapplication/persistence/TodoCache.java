package com.rapidminer.todoapplication.persistence;

import java.util.Set;

public interface TodoCache extends ToDoDao {

    void init();

    void refresh();

    Set<Long> getRemovedElements();
}
