package com.rapidminer.todoapplication.persistence;

import com.rapidminer.todoapplication.domain.ToDo;

import java.util.List;
import java.util.Optional;

public interface ToDoDao {

    List<ToDo> findInitialTodo(int amount);

    List<ToDo> findTodos(Long lastId, int amount);

    List<Long> findAllIds();

    Optional<ToDo> findById(Long id);

    Long insertTodo(ToDo toDo);

    void deleteTodo(Long id);

}
