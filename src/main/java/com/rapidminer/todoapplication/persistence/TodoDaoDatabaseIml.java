package com.rapidminer.todoapplication.persistence;

import com.rapidminer.todoapplication.domain.ToDo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.Reader;
import java.sql.Clob;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class TodoDaoDatabaseIml implements ToDoDao {
    private static final Logger logger = LoggerFactory.getLogger(TodoDaoDatabaseIml.class);
    private static final RowMapper<ToDo> rowMapper = (rs, rowNum) ->
            ToDo.ToDoBuilder
                    .aToDo()
                    .todoId(rs.getLong("TODO_ID"))
                    .subject(rs.getString("SUBJECT"))
                    .description(getClobData(rs.getClob("DESCRIPTION")))
                    .createdBy(rs.getString("CREATED_BY"))
                    .createdFor(rs.getString("CREATED_FOR"))
                    .createTime(getDateTime(rs.getTimestamp("CREATE_TIMESTAMP")))
                    .build();
    private static final String findByIdSql = "SELECT * FROM RAPIDMINER.TODO WHERE TODO_ID = ?";
    private static final String findAllIdSql = "SELECT TODO_ID FROM RAPIDMINER.TODO";
    private static final String findFirstAmountSql = "SELECT * FROM RAPIDMINER.TODO LIMIT ?";
    private static final String findNextAmountSql = "SELECT * FROM RAPIDMINER.TODO WHERE TODO_ID > ? ORDER BY TODO_ID LIMIT ? ";
    private static final String deleteTodoSql = "DELETE FROM RAPIDMINER.TODO WHERE TODO_ID = ?";
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert jdbcInsert;

    public TodoDaoDatabaseIml(DataSource dataSource) {
        this.jdbcInsert = new SimpleJdbcInsert(dataSource)
                .withTableName("RAPIDMINER.TODO")
                .usingColumns("SUBJECT", "DESCRIPTION", "CREATED_BY", "CREATED_FOR")
                .usingGeneratedKeyColumns("TODO_ID");

        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static LocalDateTime getDateTime(Timestamp timestamp) {
        return timestamp == null ? null : timestamp.toLocalDateTime();
    }

    private static String getClobData(Clob clob) {
        final StringBuilder sb = new StringBuilder();
        try {
            final Reader reader = clob.getCharacterStream();
            final BufferedReader br = new BufferedReader(reader);
            int b;
            while (-1 != (b = br.read())) {
                sb.append((char) b);
            }
            br.close();

        } catch (Exception ex) {
            logger.error("Error: ", ex);
        }
        return sb.toString();
    }

    @Override
    public List<ToDo> findInitialTodo(int amount) {
        return jdbcTemplate.query(findFirstAmountSql, rowMapper, amount);
    }

    @Override
    public List<ToDo> findTodos(Long lastId, int amount) {
        return jdbcTemplate.query(findNextAmountSql, rowMapper, lastId, amount);


    }

    @Override
    public List<Long> findAllIds() {
        return jdbcTemplate.queryForList(findAllIdSql, Long.class);
    }

    @Override
    public Long insertTodo(ToDo toDo) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("SUBJECT", toDo.getSubject());
        parameters.put("DESCRIPTION", toDo.getDescription());
        parameters.put("CREATED_BY", toDo.getCreatedBy());
        parameters.put("CREATED_FOR", toDo.getCreatedFor());

        return jdbcInsert.executeAndReturnKey(parameters).longValue();

    }

    @Override
    public void deleteTodo(Long id) {
        jdbcTemplate.update(deleteTodoSql, id);
    }

    @Override
    public Optional<ToDo> findById(Long id) {
        return jdbcTemplate.query(findByIdSql, rowMapper, id)
                .stream().findFirst();
    }
}
