package com.rapidminer.todoapplication.persistence;

import com.rapidminer.todoapplication.domain.ToDo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class TodoCacheImpl implements TodoCache {
    private static final Logger logger = LoggerFactory.getLogger(TodoCacheImpl.class);
    private final int initialLoadSize;
    private final ConcurrentMap<Long, ToDo> todoCache;
    private final Set<Long> removedCache;
    private ToDoDao toDoDao;

    public TodoCacheImpl(ToDoDao toDoDao, int initialLoadSize) {
        this.toDoDao = toDoDao;
        this.initialLoadSize = initialLoadSize;
        this.todoCache = new ConcurrentHashMap<>();
        this.removedCache = new HashSet<>();
    }

    /**
     * Block all access to the cache before initial data is ready.
     */
    @Override
    public synchronized void init() {
        logger.info("Initializing todo cache");

        toDoDao.findInitialTodo(initialLoadSize)
                .forEach(t -> todoCache.put(t.getTodoId(), t));

        logger.info("Successfully loaded {} todos", todoCache.size());
    }

    /**
     * Primitively intends to tackle scaling instances or external manipulation of data
     */
    @Scheduled(fixedDelay = 1000 * 60 * 5)
    public void refresh() {
        logger.info("Removing elements from todo cache");
        Set<Long> current = todoCache.keySet();
        Long currentMax = current.stream().max(Long::compareTo).orElse(0L);

        Set<Long> allIds = new HashSet<>(toDoDao.findAllIds());

        //keys to remove
        current.stream()
                .sorted()
                .filter(l -> !allIds.contains(l))
                .filter(l -> l <= currentMax)
                .peek(removedCache::add)
                .forEach(todoCache::remove);
    }

    @Override
    public List<ToDo> findInitialTodo(int amount) {
        if (amount > todoCache.size()) {
            findTodos(todoCache.keySet().stream().max(Long::compareTo).orElse(0L), amount);
        }
        return todoCache.keySet()
                .stream()
                .sorted()
                .limit(amount)
                .map(todoCache::get)
                .collect(Collectors.toList());
    }

    @Override
    public List<ToDo> findTodos(Long lastId, int amount) {

        long nextInCache = todoCache.keySet().stream()
                .sorted()
                .filter(key -> key > lastId)
                .limit(amount)
                .count();


        if (nextInCache < amount) {
            List<ToDo> todos = toDoDao.findTodos(lastId, amount);
            todos.forEach(t -> todoCache.put(t.getTodoId(), t));
        }

        return todoCache.keySet()
                .stream()
                .sorted()
                .filter(key -> key > lastId)
                .limit(amount)
                .map(todoCache::get)
                .collect(Collectors.toList());


    }

    @Override
    public List<Long> findAllIds() {
        return new ArrayList<>(todoCache.keySet());
    }

    @Override
    public Optional<ToDo> findById(Long id) {
        if (todoCache.containsKey(id)) {
            return Optional.ofNullable(todoCache.get(id));
        } else {
            Optional<ToDo> todo = toDoDao.findById(id);
            if (todo.isPresent()) {
                todoCache.put(id, todo.get());
                return todo;
            } else {
                return Optional.empty();
            }

        }
    }

    /**
     * Cache is refreshed via next lookup
     */
    @Override
    public Long insertTodo(ToDo toDo) {
        return toDoDao.insertTodo(toDo);
    }


    @Override
    public void deleteTodo(Long id) {

        toDoDao.deleteTodo(id);
        todoCache.remove(id);
        removedCache.add(id);
    }


    @Override
    public Set<Long> getRemovedElements() {
        return removedCache;
    }

    void setToDoDao(ToDoDao toDoDao) {
        this.toDoDao = toDoDao;
    }


}
