package com.rapidminer.todoapplication.configuration;

import com.rapidminer.todoapplication.persistence.ToDoDao;
import com.rapidminer.todoapplication.persistence.TodoCache;
import com.rapidminer.todoapplication.persistence.TodoCacheImpl;
import com.rapidminer.todoapplication.persistence.TodoDaoDatabaseIml;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;

@Configuration
public class DaoConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(DaoConfiguration.class);

    @Autowired
    private DataSource dataSource;

    @Value("${rapidminer.initialLoadSize}")
    private Integer initialLoadSize;

    @Bean
    public ToDoDao toDoDao() {
        return new TodoDaoDatabaseIml(dataSource);
    }


    @Bean(initMethod = "init")
    @Profile("!TEST")
    public TodoCache toDoCache() {
        logger.info("Creating TODO cache with initial size: {}", initialLoadSize);
        return new TodoCacheImpl(toDoDao(), initialLoadSize);
    }
}
