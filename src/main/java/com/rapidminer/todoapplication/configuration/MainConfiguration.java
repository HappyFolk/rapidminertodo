package com.rapidminer.todoapplication.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@Import({DataBaseConfiguration.class, DaoConfiguration.class})
@EnableScheduling
public class MainConfiguration {
}
