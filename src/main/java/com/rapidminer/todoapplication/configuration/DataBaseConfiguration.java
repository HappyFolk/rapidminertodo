package com.rapidminer.todoapplication.configuration;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
public class DataBaseConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(DataBaseConfiguration.class);

    @Bean(destroyMethod = "shutdown")
    @Profile("!TEST")
    public EmbeddedDatabase dataSource() {
        logger.info("Creating EMBEDDED database");
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();

        return builder.setType(EmbeddedDatabaseType.H2)
                .addScript("database/LOAD_SCHEMA.sql")
                .addScript("database/PROD_LOAD_TODO_TABLE.sql")
                .build();
    }

    @Profile("TEST")
    @Bean(name = "dataSource", destroyMethod = "shutdown")
    public EmbeddedDatabase testDataSource() {
        logger.info("Creating EMBEDDED database");
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();

        return builder.setType(EmbeddedDatabaseType.H2)
                .addScript("database/LOAD_SCHEMA.sql")
                .addScript("database/TEST_LOAD_TODO_TABLE.sql")
                .build();
    }
}
