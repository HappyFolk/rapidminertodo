package com.rapidminer.todoapplication;

import com.rapidminer.todoapplication.configuration.MainConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * Created by KD on 2018.01.13..
 */

@SpringBootApplication
@Import(MainConfiguration.class)
public class StartupMain {

    public static void main(String[] args) {
        SpringApplication.run(StartupMain.class, args);
    }
}