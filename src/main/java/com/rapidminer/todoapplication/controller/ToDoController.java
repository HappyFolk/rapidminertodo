package com.rapidminer.todoapplication.controller;


import com.rapidminer.todoapplication.domain.CreateTodoRequest;
import com.rapidminer.todoapplication.domain.StringResponseDTO;
import com.rapidminer.todoapplication.domain.ToDo;
import com.rapidminer.todoapplication.domain.UpdateTodoDTO;
import com.rapidminer.todoapplication.persistence.TodoCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/todo-endpoint")
public class ToDoController {
    private static final Logger logger = LoggerFactory.getLogger(ToDoController.class);

    @Autowired
    private TodoCache toDoCache;

    @Value("${rapidminer.initialLoadSize}")
    private Integer initialLoadSize;


    @PostMapping("/create-todo")
    public StringResponseDTO createTodo(@RequestBody CreateTodoRequest createTodoRequest) {
        logger.info("Received create request: {}", createTodoRequest);

        Long id = toDoCache.insertTodo(ToDo.ToDoBuilder
                .aToDo()
                .subject(createTodoRequest.getSubject())
                .description(createTodoRequest.getDescription())
                .createdBy(createTodoRequest.getCreatedBy())
                .createdFor(createTodoRequest.getCreatedFor())
                .build()
        );
        return new StringResponseDTO("Successfully saved todo with id: " + id);
    }

    @GetMapping("/delete-todo")
    public StringResponseDTO deleteTodo(@RequestParam Long id) {
        logger.info("Removing todo with id: {}", id);
        toDoCache.deleteTodo(id);
        return new StringResponseDTO("Successfully removed todo with id: " + id);
    }


    @GetMapping("/init-todos")
    public List<ToDo> getInitialTodos() {
        return toDoCache.findInitialTodo((initialLoadSize));
    }

    @GetMapping("/next-todos")
    public UpdateTodoDTO getNextTodos(@RequestParam Long lastId) {
        List<ToDo> nextTodos = toDoCache.findTodos(lastId, (initialLoadSize));

        UpdateTodoDTO response = new UpdateTodoDTO(nextTodos, toDoCache.getRemovedElements());
        logger.info("Next todos response: {}", response);
        return response;
    }
}
