package com.rapidminer.todoapplication.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class CreateTodoRequest {

    private final String subject;
    private final String description;
    private final String createdBy;
    private final String createdFor;

    public CreateTodoRequest(String subject, String description, String createdBy, String createdFor) {
        this.subject = subject;
        this.description = description;
        this.createdBy = createdBy;
        this.createdFor = createdFor;
    }

    public String getSubject() {
        return subject;
    }

    public String getDescription() {
        return description;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getCreatedFor() {
        return createdFor;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
