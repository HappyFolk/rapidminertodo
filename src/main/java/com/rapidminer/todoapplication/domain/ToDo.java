package com.rapidminer.todoapplication.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.util.Assert;

import java.time.LocalDateTime;

public class ToDo {
    private final Long todoId;
    private final String subject;
    private final String description;
    private final String createdBy;
    private final String createdFor;
    private final LocalDateTime createTime;

    private ToDo(Long todoId, String subject, String description,
                 String createdBy, String createdFor, LocalDateTime createTime) {
        this.todoId = todoId;
        this.subject = subject;
        this.description = description;
        this.createdBy = createdBy;
        this.createdFor = createdFor;
        this.createTime = createTime;
    }

    public Long getTodoId() {
        return todoId;
    }

    public String getSubject() {
        return subject;
    }

    public String getDescription() {
        return description;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getCreatedFor() {
        return createdFor;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }


    public static final class ToDoBuilder {
        private Long todoId;
        private String subject;
        private String description;
        private String createdBy;
        private String createdFor;
        private LocalDateTime createTime;

        private ToDoBuilder() {
        }

        public static ToDoBuilder aToDo() {
            return new ToDoBuilder();
        }

        public ToDoBuilder todoId(Long todoId) {
            this.todoId = todoId;
            return this;
        }

        public ToDoBuilder subject(String subject) {
            this.subject = subject;
            return this;
        }

        public ToDoBuilder description(String description) {
            this.description = description;
            return this;
        }

        public ToDoBuilder createdBy(String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        public ToDoBuilder createdFor(String createdFor) {
            this.createdFor = createdFor;
            return this;
        }

        public ToDoBuilder createTime(LocalDateTime createTime) {
            this.createTime = createTime;
            return this;
        }

        public ToDo build() {
            Assert.notNull(subject, "Subject cannot be null");
            Assert.isTrue(!subject.isEmpty(), "Subject cannot be empty");

            Assert.notNull(description, "Description cannot be null");
            Assert.isTrue(!description.isEmpty(), "Description cannot be empty");

            Assert.notNull(createdBy, "Reporter cannot be null");
            Assert.isTrue(!createdBy.isEmpty(), "Reporter cannot be empty");

            Assert.notNull(createdFor, "Assignee cannot be null");
            Assert.isTrue(!createdFor.isEmpty(), "Assignee cannot be empty");


            return new ToDo(todoId, subject, description, createdBy, createdFor, createTime);
        }
    }
}
