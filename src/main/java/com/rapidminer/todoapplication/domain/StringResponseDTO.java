package com.rapidminer.todoapplication.domain;

public class StringResponseDTO {
    private String message;

    public StringResponseDTO(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
