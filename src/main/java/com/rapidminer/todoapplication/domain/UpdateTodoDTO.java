package com.rapidminer.todoapplication.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;
import java.util.Set;

public class UpdateTodoDTO {
    private final List<ToDo> nextTodos;
    private final Set<Long> removedIds;

    public UpdateTodoDTO(List<ToDo> nextTodos, Set<Long> removedIds) {
        this.nextTodos = nextTodos;
        this.removedIds = removedIds;
    }

    public List<ToDo> getNextTodos() {
        return nextTodos;
    }

    public Set<Long> getRemovedIds() {
        return removedIds;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
