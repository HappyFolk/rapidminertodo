package com.rapidminer.todoapplication.persistence;

import com.rapidminer.todoapplication.domain.ToDo;
import com.rapidminer.todoapplication.test.SpringIntegrationTestBase;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class TodoDaoDatabaseImlTest extends SpringIntegrationTestBase {
    @Autowired
    private ToDoDao toDoDao;

    private static ToDo aToDo() {
        return ToDo.ToDoBuilder
                .aToDo()
                .subject("Subject")
                .description("This is description")
                .createdBy("APPLE")
                .createdFor("TREE")
                .build();
    }

    @Test
    public void testInsert() {
        ToDo toDo = aToDo();
        Long id = toDoDao.insertTodo(toDo);

        Optional<ToDo> expectedO = toDoDao.findById(id);

        assertTrue(expectedO.isPresent());
        ToDo expected = expectedO.get();

        assertEquals(expected.getCreatedBy(), toDo.getCreatedBy());
        assertEquals(expected.getCreatedFor(), toDo.getCreatedFor());
        assertEquals(expected.getSubject(), toDo.getSubject());
        assertEquals(expected.getDescription(), toDo.getDescription());
        assertEquals(expected.getTodoId(), id);
        assertNotNull(expected.getCreateTime());
    }

    @Test
    public void testDelete() {
        ToDo toDo = aToDo();
        Long id = toDoDao.insertTodo(toDo);
        toDoDao.deleteTodo(id);
        assertFalse(toDoDao.findById(id).isPresent());
    }

    @Test
    public void testFindFirst() {
        List<ToDo> initialTodo = toDoDao.findInitialTodo(3);

        List<Long> actual = initialTodo.stream()
                .map(ToDo::getTodoId)
                .sorted()
                .collect(Collectors.toList());

        List<Long> expected = Arrays.asList(1L, 2L, 3L);

        assertEquals(expected, actual);
    }

    @Test
    public void testFindRange() {
        List<ToDo> todos = toDoDao.findTodos(3L, 10);

        List<Long> actual = todos.stream()
                .map(ToDo::getTodoId)
                .sorted()
                .collect(Collectors.toList());

        List<Long> expected = Arrays.asList(4L, 5L, 6L);

        assertEquals(expected, actual);
    }

    @Test
    public void testFindIds() {
        List<Long> actual = toDoDao.findAllIds();
        System.out.println(actual);

        List<Long> expected = Arrays.asList(1L, 2L, 3L, 4L, 5L, 6L);
        assertEquals(expected, actual);
    }
}