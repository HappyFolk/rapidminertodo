package com.rapidminer.todoapplication.persistence;

import com.rapidminer.todoapplication.domain.ToDo;
import com.rapidminer.todoapplication.test.SpringIntegrationTestBase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


public class TodoCacheImplTest extends SpringIntegrationTestBase {


    private static final int INITIAL_LOAD_SIZE = 3;
    @Autowired
    private ToDoDao toDoDao;
    private ToDoDao mockDao;
    private TodoCache classUnderTest;

    @Before
    public void setup() {
        mockDao = Mockito.mock(ToDoDao.class);

        classUnderTest = new TodoCacheImpl(toDoDao, INITIAL_LOAD_SIZE);
        classUnderTest.init();


    }

    @Test
    public void testInitialLookupFromCache1() {


        //Mock out further interaction to the DAO
        ((TodoCacheImpl) classUnderTest).setToDoDao(mockDao);

        List<ToDo> initialTodos = classUnderTest.findInitialTodo(2);
        verify(mockDao, times(0)).findTodos(anyLong(), anyInt());

        List<Long> actual = initialTodos.stream()
                .map(ToDo::getTodoId)
                .sorted()
                .collect(Collectors.toList());


        assertEquals(Arrays.asList(1L, 2L), actual);
    }

    @Test
    public void testInitialLookupFromCache2() {
        List<ToDo> initialTodos = classUnderTest.findInitialTodo(4);

        List<Long> actual = initialTodos.stream()
                .map(ToDo::getTodoId)
                .sorted()
                .collect(Collectors.toList());


        assertEquals(Arrays.asList(1L, 2L, 3L, 4L), actual);
    }


    @Test
    public void testInitialLookupFromCache3() {
        classUnderTest.deleteTodo(1L);
        List<ToDo> initialTodos = classUnderTest.findInitialTodo(4);

        List<Long> actual = initialTodos.stream()
                .map(ToDo::getTodoId)
                .sorted()
                .collect(Collectors.toList());


        assertEquals(Arrays.asList(2L, 3L, 4L, 5L), actual);
    }

    @Test
    public void testRangeLookupFromCache() {
        //Mock out further interaction to the DAO
        ((TodoCacheImpl) classUnderTest).setToDoDao(mockDao);

        List<ToDo> initialTodos = classUnderTest.findTodos(0L, 2);
        verify(mockDao, times(0)).findTodos(anyLong(), anyInt());

        List<Long> actual = initialTodos.stream()
                .map(ToDo::getTodoId)
                .sorted()
                .collect(Collectors.toList());


        assertEquals(Arrays.asList(1L, 2L), actual);
    }

    @Test
    public void testRangeLookupExpandingCache() {
        List<ToDo> initialTodos = classUnderTest.findTodos(2L, 3);

        List<Long> actual = initialTodos.stream()
                .map(ToDo::getTodoId)
                .sorted()
                .collect(Collectors.toList());


        assertEquals(Arrays.asList(3L, 4L, 5L), actual);
    }

    @Test
    public void testExternalRemoveAndRefresh() {

        //External Interaction
        toDoDao.deleteTodo(1L);
        toDoDao.deleteTodo(2L);

        classUnderTest.refresh();

        List<Long> actual = classUnderTest.findAllIds();
        assertEquals(Collections.singletonList(3L), actual);
    }

    @Test
    public void testFindByIdExpand() {
        classUnderTest.findById(5L);
        assertEquals(Arrays.asList(1L, 2L, 3L, 5L), classUnderTest.findAllIds());
    }
}