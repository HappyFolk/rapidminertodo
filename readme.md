Repository available at: https://bitbucket.org/HappyFolk/rapidminertodo/src/master/


Run application:

1. gradlew build docker

2. docker run -p 8081:8081 -t rapidminer/gs-spring-boot



Technology considerations:

1. I've intended to represent the aspect of my previous work environment rather then showing something entirely different.

2. Database Layer: i have experience with spring-data. I wanted to show more code compared to everything set up by Spring. Even H2 tables.

3. Rest Layer: I'm aware of Spring capable of creating a CRUD service and handle pagination. I've also chosen to show more code with caching implementation. Also for the ability to dynamically handle removed elements on the server side.


Assignment considerations:

1. Implementing lazy load implies the users require data to be shown quickly.

2. For that reason data should be delivered to the UI quickly in small chunks.

3. For the same reason a server side cache is used to retrieve from DB in small chunks, reuse that has been previously access, and consider a TODO can be anything, even a book. (clob)
 